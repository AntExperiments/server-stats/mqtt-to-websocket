FROM frolvlad/alpine-glibc
RUN apk add --no-cache libgcc

ADD target/release/mqtt-to-websocket /app

EXPOSE 9001/tcp

CMD /app