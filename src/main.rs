use ws::{listen, Handler, Message, Response, Request, connect, CloseCode, Result, Sender};
use futures::{executor::block_on, stream::StreamExt};
use std::{env, process, time::Duration};
use paho_mqtt as mqtt;
use async_std::task;

use dict::{ Dict, DictIface };

const MQTT_HOST: &str = "tcp://192.168.1.12:1883";
const MQTT_NAME: &str = "ws-bridge";
const WS_PORT: &str = "9001";

const MQTT_TOPICS: &[&str] = &[
    "docker/storage/root/used",
    "docker/storage/root/total",
    "docker/containers/running",
    "hera/storage/used"
];

static mut LAST_RESPONSES: Dict<String> = Dict::<String>::new();

fn main() {
    task::spawn(async {
        mqtt()
    });

    listen(format!("0.0.0.0:{}", WS_PORT), |out| Server { out }).unwrap()
}

fn mqtt() {
    // #region Create Client and Connection

    // Create the client
    let host = env::args().nth(1) .unwrap_or_else(|| MQTT_HOST.to_string());
    let create_opts = mqtt::CreateOptionsBuilder::new()
        .server_uri(host)
        .client_id(MQTT_NAME)
        .finalize();

    // Create the client connection
    let mut cli = mqtt::AsyncClient::new(create_opts).unwrap_or_else(|e| {
        println!("Error creating the client: {:?}", e);
        process::exit(1);
    });

    // #endregion

    if let Err(err) = block_on(async {
        // Get message stream before connecting.
        let mut stream = cli.get_stream(25);

        // #region Connection

        let conn_opts = mqtt::ConnectOptionsBuilder::new()
            .keep_alive_interval(Duration::from_secs(600))
            .mqtt_version(mqtt::MQTT_VERSION_3_1_1)
            .clean_session(false)
            .finalize();

        // Make the connection to the broker
        println!("Connecting to the MQTT server...");
        cli.connect(conn_opts).await?;

        println!("Subscribing to topics: {}", MQTT_TOPICS.join(", "));
        cli.subscribe_many(MQTT_TOPICS, &[1; MQTT_TOPICS.len()]).await?;

        // #endregion

        // #region Main Loop

        println!("Start Successful :)");
        while let Some(msg_opt) = stream.next().await {
            if let Some(msg) = msg_opt {
                let topic = msg.topic();
                let payload = msg.payload_str();

                publish_change(topic, &payload);
            }
            else {
                println!("Lost connection. Attempting reconnect.");
                while let Err(err) = cli.reconnect().await {
                    println!("Error reconnecting: {}", err);
                    async_std::task::sleep(Duration::from_millis(1000)).await;
                }
            }
        }

        // #endregion

        // Explicit return type for the async block
        Ok::<(), mqtt::Error>(())
    }) {
        eprintln!("{}", err);
    }
}

struct Server {
    out: Sender
}

impl Handler for Server {
    fn on_request(&mut self, req: &Request) -> Result<Response> {

        match req.resource() {
            _ => {
                // send every last sent value on first connect
                // TODO: Don't make it send these to itself
                //       (aka, maybe don't connect to yourself in the first place)
                unsafe {
                    for o in &LAST_RESPONSES {
                        let _ = self.out.send(
                            format!("{}: {}", o.key, o.val)
                        );
                    }
                }

                // finalize connection
                return Response::from_request(req);
            },
        }

    }

    fn on_message(&mut self, msg: Message) -> Result<()> {
        // TODO: Check to make sure only some clients can send messages
        self.out.broadcast(msg)
    }
}

fn publish_change(topic: &str, payload: &str) {
    println!("{} = {}", topic, payload);

    // save current value in dict
    unsafe {
        if LAST_RESPONSES.contains_key(topic.clone()) {
            LAST_RESPONSES.remove_key(topic.clone());
        }
        LAST_RESPONSES.add(topic.clone().to_string(), payload.clone().to_string());
    }

    if let Err(error) = connect(format!("ws://127.0.0.1:{}", WS_PORT), |out| {
        let message_string = format!("{}: {}", topic, payload);

        // Queue a message to be sent when the WebSocket is open
        if out.send(message_string.clone()).is_err() {
            println!("Websocket couldn't queue an initial message.")
        } else {
            println!("Client sent message '{}'", message_string)
        }

        // The handler needs to take ownership of out, so we use move
        move |_| {
            out.close(CloseCode::Normal)
        }
    }) {
        // Inform the user of failure
        println!("Failed to create WebSocket due to: {:?}", error);
    }
}